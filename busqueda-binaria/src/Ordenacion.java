
public class Ordenacion {


    public static void main(String[] args)
    {
        // En este clases se ejecutaran todos los metodos de ordenacion
        int[] arreglo = {5,2,42,6,1,3,2};
        burbuajaDecendente(arreglo);
       // burbuajaAcendente(arreglo);
        imprimirArreglo(arreglo);

    }
    static void burbuajaAcendente(int[] arreglo ){

        boolean intercambio = true;
        for(int i = 0; i < arreglo.length; i++){
            intercambio = false;
            for(int j = 0 ; j < arreglo.length-i-1; j++){
                if(arreglo[j] > arreglo[j+1]){
                    int temporal =arreglo[j];
                    arreglo[j] = arreglo[j+1];
                    arreglo[j+1] = temporal;
                    intercambio = true;
                }
            }
            if(intercambio == false)
                break;
        }
    }

    static void burbuajaDecendente(int[] arreglo ){

        boolean intercambio = true;
        for(int i = arreglo.length-1; i >=0; i--){
            intercambio = false;
            for(int j = arreglo.length-1 ; j >arreglo.length-i-1; j--){
                if(arreglo[j] > arreglo[j-1]){
                    int temporal =arreglo[j];
                    arreglo[j] = arreglo[j-1];
                    arreglo[j-1] = temporal;
                    intercambio = true;
                }
            }
            if(intercambio == false)
                break;
        }
    }
    static void imprimirArreglo(int[] arreglo){
        for (int i = 0; i < arreglo.length; i++){
            System.out.println(arreglo[i]+ " ");
        }
    }
}
